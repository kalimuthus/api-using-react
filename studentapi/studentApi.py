from http.server import HTTPServer, SimpleHTTPRequestHandler
import json

allStudents=[{"firstName": "Kali",
         "lastName": "s",
         "emailId": "kgp@car.com",
         "address":"123 street,kovai",
         "gender":"male"}]


# Defining a HTTP request Handler class
class echoHandler(SimpleHTTPRequestHandler):
    def end_headers(self):
        # overwrite SimpleHTTPRequestHandler to handle CORS policy
        self.send_header('Access-Control-Allow-Origin', "*")
        self.send_header("Access-Control-Allow-Headers", "*")
        self.send_header('Access-Control-Allow-Methods', '*')
        SimpleHTTPRequestHandler.end_headers(self)
    
    def do_GET(self):
        if self.path=="/getAllStudents":
            self.send_response(200)
            self.send_header('Content-type', '*/*')
            self.end_headers()
            self.wfile.write(json.dumps(allStudents).encode())
  
    def do_POST(self):
        if self.path=="/addStudent":
            self.send_response(200)
            self.send_header('Content-type', '*/*')
            length = int(self.headers['Content-Length'])
            content = json.loads(self.rfile.read(length))
            self.end_headers()
            b=True
            for s in allStudents:
                if(s["emailId"]==content["emailId"]):
                    b=False
                    self.wfile.write(json.dumps({'message':"Students emailId name already exits"}).encode())
                    break
            if(b):
                allStudents.append(content)
                self.wfile.write(json.dumps(allStudents).encode())


def main():
    PORT=8080
    server=HTTPServer(('localhost',PORT),echoHandler)
    print('Server running port %s' % PORT)
    server.serve_forever()



if __name__ == '__main__':
    main()
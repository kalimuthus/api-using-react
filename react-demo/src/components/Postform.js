import  React, { useState } from 'react';
import Axios from 'axios';
function  Postform(){
    //const url="https://dummy.restapiexample.com/api/v1/create";
    const[data,setData]=useState({
       firstName:"",
       lastName:"",
       emailId:"",
       address:'',
       gender:""
    }) 
    function submit(e){

        Axios.post("http://localhost:8080/addStudent",{
            firstName:data.firstName,
            lastName:data.lastName,
            emailId:data.emailId,
            address:data.address,
            gender:data.gender
        }, {headers: {
            'Content-Type': 'text/plain'
            }})
        .then(res =>{
            console.log(res.data)
        }).catch(error => console.log(error))

    }
    function handle(e){
        const newdata={...data}
        newdata[e.target.id]=e.target.value
        setData(newdata)
        console.log(newdata)
          
    }
    return(
        <div>

            <form onSubmit={(e)=>submit(e)}> 
                <pre>
                <h1 align="center"><input onChange={(e)=> handle(e)} id="firstName"        value={data.firstName}  placeholder="firstName"   type="text"></input>      <br/></h1>
                <h2 align="center"><input onChange={(e)=> handle(e)} id="lastName"          value={data.lastName}    placeholder="lastName"  type="text"></input>           <br/></h2>
                <h2 align="center"><input onChange={(e)=> handle(e)} id="emailId"        value={data.emailId}placeholder="emailId"     type="text"></input>    <br/></h2>
                <h2 align="center"><input onChange={(e)=> handle(e)} id="address"        value={data.address}placeholder="address"     type="text"></input>    <br/></h2>
                <h2 align="center"><input onChange={(e)=> handle(e)} id="gender"        value={data.gender}placeholder="gender"     type="text"></input>    <br/></h2>
               <button>Submit</button>

                </pre>
            </form>
            

        </div>
    );
}
export default Postform;
import  React, { useState } from 'react';
import Axios from 'axios';
import '../App.css';
function  GetData(){
    //const url="https://dummy.restapiexample.com/api/v1/create";
    const[data,setData]=useState([]) 
    function getUser(){

        Axios.get("http://localhost:8080/getAllStudents")
        .then(res=>{
            console.log(res)
            setData(res.data)
        }).catch(error => console.log(error))

    }
 
    return(
        <div>

           <button onClick={getUser}>Get User</button>
           <table>
               <thead>
           
               <tr>
                   <th>First Name</th>
                   <th>Last Name</th>
                   <th>Email Id</th>
                   <th>Address</th>
                   <th>Gender</th>
                </tr>
                </thead>
                <tbody>
                {data.map(d =>(
                    <tr>
                        <td>{d.firstName}</td>
                        <td>{d.lastName}</td>
                        <td>{d.emailId}</td>
                        <td>{d.address}</td>
                        <td>{d.gender}</td>
                    </tr>
                
                    
                ))
            }
                </tbody>
                    
            </table>
        </div>
    );
}
export default GetData;
import React from 'react';
import Postform from './components/Postform'
import GetData from './components/Getmethod'

function App(){
      return (
            <div className="App">
            <Postform/>
            <GetData/>
            </div>
      );
      }     
export default App;